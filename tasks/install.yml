---


- name: Create {{ go_app_name }} group
  group:
    name: "{{ go_app_user_group }}"
    system: yes


- name: Create {{ go_app_name }} user
  user:
    name: "{{ go_app_user }}"
    shell: /usr/sbin/nologin
    group: "{{ go_app_user_group }}"
    home: /nonexistent
    createhome: no
    system: yes


- name: Create directory structure
  file:
    state: directory
    mode: 0755
    owner: "{{ go_app_user }}"
    group: "{{ go_app_user_group }}"
    path: "{{ item }}"
  with_items: "{{ go_app_directories }}"


- name: Install {{ go_app_name }} requirements
  package:
    name: "{{ go_app_install_requirements + ['logrotate'] }}"
    state: present


- name: Download {{ go_app_name }} distro
  get_url:
    url: "{{ go_app_download_url }}"
    dest: /usr/src


- name: Install {{ go_app_name }}
  block:

    - name: Make folder for unarchived files
      tempfile:
        state: directory
        suffix: "_{{ go_app_name }}_{{ go_app_version }}"
      register: _go_app_distro_unarchived


    - name: Unarchive distro
      unarchive:
        remote_src: yes
        src: "/usr/src/{{ go_app_download_url | basename }}"
        dest: "{{ _go_app_distro_unarchived.path }}"


    - name: Search unarchived folders
      find:
        file_type: directory
        recurse: no
        paths: "{{ _go_app_distro_unarchived.path }}"
      register: _go_app_distro


    - name: Copy distro files
      copy:
        src: >-
          {% if _go_app_distro.matched == 1 -%}
          {{ _go_app_distro.files | map(attribute='path') | first }}/
          {%- else -%}
          {{ _go_app_distro_unarchived.path }}/
          {%- endif %}
        dest: "{{ go_app_dirpath }}/"
        owner: "{{ go_app_user }}"
        group: "{{ go_app_user_group }}"
        remote_src: yes


    - name: Change binaries mode to executable
      file:
        path: "{{ go_app_dirpath }}/{{ item }}"
        mode: "+x"
      with_items: "{{ go_app_binaries }}"

  always:

    - name: Remove distro archive and files
      file:
        state: absent
        path: "{{ item }}"
      with_items:
        - "/usr/src/{{ go_app_download_url | basename }}"
        - "{{ _go_app_distro_unarchived.path }}"
